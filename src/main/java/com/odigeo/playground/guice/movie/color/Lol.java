package com.odigeo.playground.guice.movie.color;

import com.odigeo.playground.guice.movie.types.ColoredString;
import com.odigeo.playground.guice.movie.Movie;

import java.util.Arrays;
import java.util.List;

import static com.odigeo.playground.guice.movie.types.Color.BLUE_UNDERLINED;
import static com.odigeo.playground.guice.movie.types.Color.GREEN_BOLD_BRIGHT;
import static com.odigeo.playground.guice.movie.types.Color.MAGENTA_BOLD_BRIGHT;
import static com.odigeo.playground.guice.movie.types.Color.RED_UNDERLINED;
import static com.odigeo.playground.guice.movie.types.Color.YELLOW_BOLD;

public class Lol implements Movie<ColoredString> {

    private final List<ColoredString> strip = Arrays.asList(
            new ColoredString(RED_UNDERLINED, "Haha"),
            new ColoredString(YELLOW_BOLD, "Hahahaha"),
            new ColoredString(GREEN_BOLD_BRIGHT, "UAAAHAHAHAHAHAH!"),
            new ColoredString(BLUE_UNDERLINED, "OMG!!"),
            new ColoredString(MAGENTA_BOLD_BRIGHT, "LOL!!!")
    );

    public List<ColoredString> getStrip() {
        return strip;
    }
}
