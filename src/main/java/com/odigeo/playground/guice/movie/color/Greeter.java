package com.odigeo.playground.guice.movie.color;

import com.odigeo.playground.guice.movie.types.ColoredString;
import com.odigeo.playground.guice.movie.Movie;

import java.util.Arrays;
import java.util.List;

import static com.odigeo.playground.guice.movie.types.Color.GREEN_BRIGHT;
import static com.odigeo.playground.guice.movie.types.Color.WHITE;
import static com.odigeo.playground.guice.movie.types.Color.WHITE_BOLD;
import static com.odigeo.playground.guice.movie.types.Color.WHITE_UNDERLINED;

public class Greeter implements Movie<ColoredString> {

    private final List<ColoredString> strip = Arrays.asList(
            new ColoredString(WHITE, "Welcome"),
            new ColoredString(WHITE_BOLD, "to"),
            new ColoredString(WHITE_UNDERLINED, "Google"),
            new ColoredString(WHITE_UNDERLINED, "Guice"),
            new ColoredString(GREEN_BRIGHT, "playground!")
    );

    public List<ColoredString> getStrip() {
        return strip;
    }
}
