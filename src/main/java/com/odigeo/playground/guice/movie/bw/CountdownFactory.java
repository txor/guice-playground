package com.odigeo.playground.guice.movie.bw;

import com.odigeo.playground.guice.movie.Movie;

/**
 * This factory provides the means for fine-tune build Countdown {@link Movie}s of {@link String}.
 * It allows the providing of construct-time values (annotated with @AssitedInject).
 *
 * @see <a href="https://github.com/google/guice/wiki/AssistedInject">Guice AssistedInject</a>
 */
public interface CountdownFactory {

    Countdown build(String yell);

    Countdown build(String yell, int amount);
}
