package com.odigeo.playground.guice.movie;

import java.util.List;

public interface Movie<T> {

    List<T> getStrip();
}
