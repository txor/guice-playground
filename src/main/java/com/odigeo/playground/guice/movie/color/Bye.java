package com.odigeo.playground.guice.movie.color;

import com.odigeo.playground.guice.movie.types.ColoredString;
import com.odigeo.playground.guice.movie.Movie;

import java.util.Arrays;
import java.util.List;

import static com.odigeo.playground.guice.movie.types.Color.WHITE_BOLD;
import static com.odigeo.playground.guice.movie.types.Color.WHITE_UNDERLINED;
import static com.odigeo.playground.guice.movie.types.Color.YELLOW_BOLD_BRIGHT;

public class Bye implements Movie<ColoredString> {

    private final List<ColoredString> strip = Arrays.asList(
            new ColoredString(WHITE_UNDERLINED, "Thanks"),
            new ColoredString(WHITE_BOLD, "for"),
            new ColoredString(WHITE_BOLD, "watching"),
            new ColoredString(YELLOW_BOLD_BRIGHT, "Bye Bye!")
    );

    public List<ColoredString> getStrip() {
        return strip;
    }
}
