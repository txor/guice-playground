package com.odigeo.playground.guice.movie.types;

public class ColoredString {

    private final Color color;
    private final String value;

    public ColoredString(Color color, String value) {
        this.color = color;
        this.value = value;
    }

    @Override
    public String toString() {
        return color + value;
    }

    public int length() {
        return value.length();
    }
}
