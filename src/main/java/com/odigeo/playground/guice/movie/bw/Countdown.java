package com.odigeo.playground.guice.movie.bw;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.google.inject.name.Named;
import com.odigeo.playground.guice.movie.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * A countdown {@link Movie} generator.
 * The amount and the yelling can be configured.
 *
 * @see <a href="https://github.com/google/guice/wiki/AssistedInject">Guice AssistedInject</a>
 */
public class Countdown implements Movie<String> {

    private final List<String> strip;
    private final int amount;
    private final String yell;

    @AssistedInject
    Countdown(@Assisted String yell, @Named("count to three") Integer amount) {
        this.yell = yell;
        this.amount = amount;
        strip = generateMovie();
    }

    @AssistedInject
    Countdown(@Assisted String yell, @Assisted int amount) {
        this.yell = yell;
        this.amount = amount;
        strip = generateMovie();
    }

    public List<String> getStrip() {
        return strip;
    }

    private List<String> generateMovie() {
        List<String> movie = new ArrayList<String>();
        for (int i = amount; i > 0; i--) {
            movie.add(Integer.toString(i));
        }
        movie.add(yell.toUpperCase() + "!");
        return movie;
    }
}
