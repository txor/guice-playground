package com.odigeo.playground.guice.player;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.playground.guice.config.GuiceConfigurationModule;
import com.odigeo.playground.guice.movie.Movie;
import com.odigeo.playground.guice.movie.types.ColoredString;
import com.odigeo.playground.guice.printers.Printer;

/**
 * This PlayerEngine composes and plays a {@link Movie}.
 * It is configured as a Singleton and takes advantage of the TypeLiteral configuration done in {@link GuiceConfigurationModule}
 *
 * @see <a href="https://github.com/google/guice/wiki/Scopes">Guice Scopes</a>
 * @see <a href="http://google.github.io/guice/api-docs/4.2/javadoc/com/google/inject/TypeLiteral.html">TypeLiteral</a>
 */
@Singleton
public class PlayerEngine {

    private static final int DEFAULT_SPEED = 250;
    private static final int ONE_LOOP = 1;

    private int speed = DEFAULT_SPEED;
    private int loops = ONE_LOOP;

    private final Printer<String> basicPrinter;
    private final Printer<ColoredString> coloredPrinter;

    @Inject
    public PlayerEngine(Printer basicPrinter, Printer<ColoredString> coloredPrinter) {
        this.basicPrinter = basicPrinter;
        this.coloredPrinter = coloredPrinter;
    }

    public void playSequence(Movie<?> scene) {
        Printer printer = getPrinter(scene);
        for (Object frame : scene.getStrip()) {
            printer.print(frame);
            waitCadence();
        }
        printer.finish();
    }

    public void playLoop(Movie<?> scene) {
        Printer printer = getPrinter(scene);
        for (int i = 0; i < loops; i++) {
            for (Object frame : scene.getStrip()) {
                printer.print(frame);
                waitCadence();
                printer.delete(frame);
            }
        }
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setDefaultCadence() {
        this.speed = DEFAULT_SPEED;
    }

    public void setLoops(int loops) {
        this.loops = loops;
    }

    public void setOneLoop() {
        this.loops = ONE_LOOP;
    }

    private void waitCadence() {
        try {
            Thread.sleep(speed);
        } catch (InterruptedException ignored) {
        }
    }

    private Printer getPrinter(Movie<?> movie) {
        if (movie.getStrip().get(0) instanceof ColoredString) {
            return coloredPrinter;
        } else {
            return basicPrinter;
        }

    }
}
