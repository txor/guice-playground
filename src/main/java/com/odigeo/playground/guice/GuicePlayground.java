package com.odigeo.playground.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odigeo.playground.guice.config.GuiceConfigurationModule;
import com.odigeo.playground.guice.movie.Movie;
import com.odigeo.playground.guice.movie.bw.CountdownFactory;
import com.odigeo.playground.guice.movie.color.Bye;
import com.odigeo.playground.guice.movie.color.Greeter;
import com.odigeo.playground.guice.movie.color.Lol;
import com.odigeo.playground.guice.movie.types.ColoredString;
import com.odigeo.playground.guice.player.PlayerEngine;

public class GuicePlayground {

    private static final Injector guiceInjector = Guice.createInjector(new GuiceConfigurationModule());

    public static void main(String[] args) {

        player().playSequence(coloredMovie(Greeter.class));

        player().setSpeed(oneSecond());
        player().playSequence(countdown().build("go"));

        player().setDefaultCadence();
        player().setLoops(3);
        player().playLoop(coloredMovie(Lol.class));

        player().setOneLoop();
        player().setSpeed(oneSecond());
        player().playLoop(countdown().build("boom", 5));

        player().setDefaultCadence();
        player().playSequence(coloredMovie(Bye.class));
    }

    /**
     * Provides a Singleton PlayerEngine
     *
     * @see <a href="https://github.com/google/guice/wiki/Scopes">Guice Scopes</a>
     */
    private static PlayerEngine player() {
        return guiceInjector.getInstance(PlayerEngine.class);
    }

    /**
     * Provides a flexible factory which is configured with @AssistedInject and @Assisted
     *
     * @see <a href="https://github.com/google/guice/wiki/AssistedInject">Guice AssistedInject</a>
     */
    private static CountdownFactory countdown() {
        return guiceInjector.getInstance(CountdownFactory.class);
    }

    /**
     * Provides a {@link Movie} of {@link ColoredString} which is configured using the generic type representation capability of Guice
     *
     * @see <a href="http://google.github.io/guice/api-docs/4.2/javadoc/com/google/inject/TypeLiteral.html">TypeLiteral</a>
     */
    private static Movie<ColoredString> coloredMovie(Class<? extends Movie<ColoredString>> movie) {
        return guiceInjector.getInstance(movie);
    }

    /**
     * Provides the {@link Integer} value of 1000 using the Guice-configured Instance Binding at {@link GuiceConfigurationModule}
     *
     * @see <a href="https://github.com/google/guice/wiki/InstanceBindings">Instance Bindings</a>
     */
    private static Integer oneSecond() {
        return guiceInjector.getInstance(Integer.class);
    }
}
