package com.odigeo.playground.guice.config;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.odigeo.playground.guice.movie.bw.Countdown;
import com.odigeo.playground.guice.movie.bw.CountdownFactory;
import com.odigeo.playground.guice.movie.types.ColoredString;
import com.odigeo.playground.guice.printers.ColoredStringPrinter;
import com.odigeo.playground.guice.printers.Printer;

/**
 * This Module provides almost all the Guice configuration used in this playground
 *
 * @see <a href="https://github.com/google/guice/wiki/Bindings">Guice Bindings</a>
 * @see <a href="https://github.com/google/guice/wiki/InstanceBindings">Instance Bindings</a>
 * @see <a href="https://github.com/google/guice/wiki/BindingAnnotations">Guice Binding Annotations</a>
 * @see <a href="http://google.github.io/guice/api-docs/4.2/javadoc/com/google/inject/TypeLiteral.html">TypeLiteral</a>
 * @see <a href="https://github.com/google/guice/wiki/AssistedInject">AssistedInjects</a>
 */
public class GuiceConfigurationModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Integer.class).toInstance(1000);
        bind(Integer.class).annotatedWith(Names.named("count to three")).toInstance(3);
        bind(new TypeLiteral<Printer<ColoredString>>() {}).toInstance(new ColoredStringPrinter());
        install(new FactoryModuleBuilder().implement(Countdown.class, Countdown.class).build(CountdownFactory.class));
    }
}
