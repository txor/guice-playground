package com.odigeo.playground.guice.printers;

import com.odigeo.playground.guice.movie.types.ColoredString;

import static com.odigeo.playground.guice.printers.Console.BACKSPACE_CHARACTER;
import static com.odigeo.playground.guice.printers.Console.NULL_CHARACTER;
import static com.odigeo.playground.guice.printers.Console.RESET;

public class ColoredStringPrinter implements Printer<ColoredString> {

    public void print(ColoredString frame) {
        System.out.print(frame + RESET + " ");
    }

    public void delete(ColoredString frame) {
        System.out.print(new String(new char[frame.length() + 1]).replace(NULL_CHARACTER, BACKSPACE_CHARACTER));
    }

    public void finish() {
        System.out.println();
    }
}
