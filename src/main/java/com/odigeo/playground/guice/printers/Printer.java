package com.odigeo.playground.guice.printers;

import com.google.inject.ImplementedBy;

/**
 * Generic console printing service with it's default implementation
 *
 * @see <a href="https://github.com/google/guice/wiki/JustInTimeBindings#implementedby">Guice ImplementedBy annotation</a>
 */
@ImplementedBy(StringPrinter.class)
public interface Printer<T> {

    void print(T frame);

    void delete(T frame);

    void finish();
}
