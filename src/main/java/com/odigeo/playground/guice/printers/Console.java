package com.odigeo.playground.guice.printers;

class Console {

    static final String RESET = "\033[0m";
    static final String NULL_CHARACTER = "\0";
    static final String BACKSPACE_CHARACTER = "\u0008";

    private Console() {
    }
}
