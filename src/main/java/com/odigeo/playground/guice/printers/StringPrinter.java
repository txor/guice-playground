package com.odigeo.playground.guice.printers;

public class StringPrinter implements Printer<String> {

    public void print(String frame) {
        System.out.print(frame + " ");
    }

    public void delete(String frame) {
        System.out.print(new String(new char[frame.length() + 1]).replace(Console.NULL_CHARACTER.toString(), Console.BACKSPACE_CHARACTER.toString()));
    }

    public void finish() {
        System.out.println();
    }
}
