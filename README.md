# Guice Playground

This project is a playground where you can learn and practice with the Guice framework.
With this excuse in hand, the demo has the ability to print Scenes to console in an animated way.

Don't forget to take a look at the [official Guice documentation](https://github.com/google/guice/wiki)!